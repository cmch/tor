* Dockerfile

C-u C-c C-v t


#+HEADER: :tangle Dockerfile
#+BEGIN_SRC sh

FROM alpine

RUN apk --no-cache add \
tor

#+END_SRC


* tag

|         | alpine | tor        |                        |
|---------+--------+------------+------------------------|
| 0.3.5.8 | v3.10  | 0.3.5.8-r0 | [2019-07-19 Fri 09:15] |


* errors

** How do you trigger a build for an existing tag?

Answer
Currently the only way to trigger a rebuild for a tag on Docker Hub is:

Remove the tag in Git
Push the repository
Add the tag back to Git
Push the repository again
Note: there is an open feature request for this at https://github.com/docker/hub-feedback/issues/620



https://success.docker.com/article/trigger-a-build-for-an-automated-build-tag


